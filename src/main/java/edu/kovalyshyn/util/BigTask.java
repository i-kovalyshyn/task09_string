package edu.kovalyshyn.util;

import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class BigTask {

    //Знайти таке слово у першому реченні, якого немає ні в одному з інших речень.
    public static String findUniqueWordInFirstSentence(List<String> text) {
        String firstSentence = text.get(0);
        String[] firstSentenceWords = firstSentence.split(" ");

        String uniqueWord = "";
        for (String sentence : text) {
            String[] words = sentence.split(" ");
            for (String firstSentenceWord : firstSentenceWords) {
                for (String word : words) {
                    if (!word.equals(firstSentenceWord)) {
                        uniqueWord = firstSentenceWord;
                    }
                }
            }
        }
        return uniqueWord;
    }

    //У всіх запитальних реченнях тексту знайти і надрукувати без повторів слова заданої довжини.
    public static List<String> questionsUniqueWords(List<String> text, int length) {
        List<String> questions = text
                .stream()
                .filter(s -> s.endsWith("?"))
                .collect(Collectors.toList());
        List<String> uniqueQuestionsWords = questions
                .stream()
                .distinct()
                .collect(Collectors.toList());
        return uniqueQuestionsWords
                .stream()
                .filter(q -> q.length() == length)
                .collect(Collectors.toList());
    }

    //У кожному реченні тексту поміняти місцями перше слово, що починається на голосну букву з найдовшим словом.
    public static List<String> replaceWords(List<String> text) {
        for (String sentence : text) {
            String firstWordStartsWithVowel = "";
            String[] words = sentence.split(" ");
            for (String w : words) {
                if (w.toLowerCase().startsWith("[aeiouy]")) {
                    firstWordStartsWithVowel = w;
                }
            }
            String longestWord = Arrays.stream(words)
                    .max(Comparator.comparingInt(String::length))
                    .get();

            int firstWordStartsWithVowelIndex = text.indexOf(firstWordStartsWithVowel);
            int longestWordIndex = text.indexOf(longestWord);

            if (firstWordStartsWithVowelIndex > 0 && longestWordIndex > 0) {
                text.add(firstWordStartsWithVowelIndex, longestWord);
                text.add(longestWordIndex, firstWordStartsWithVowel);
            }
        }
        return text;
    }

    //Відсортувати слова тексту за зростанням відсотку голосних букв
    //(співвідношення кількості голосних до загальної кількості букв у слові).
    public static List<String> sortByVowelsPercentage(List<String> text) {
        List<String> sorted = new ArrayList<>();
        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            words.sort(new VowelsPercentageComparator());
            sorted.addAll(words);
        }
        sorted.sort(new VowelsPercentageComparator());
        return sorted;
    }

    static class VowelsPercentageComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            int o1VowelsPercentage = countVowelsPercentage(o1);
            int o2VowelsPercentage = countVowelsPercentage(o2);
            if (o1VowelsPercentage > o2VowelsPercentage) {
                return 1;
            } else if (o1VowelsPercentage == o2VowelsPercentage) {
                return 0;
            }
            return -1;
        }
    }

    private static int countVowelsPercentage(String word) {
        int vowelCount = 0;
        int vowelsPercentage = 0;
        for (char c : word.toCharArray()) {
            switch (c) {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                case 'y':
                    vowelCount++;
                    break;
            }
            if (vowelCount != 0) {
                vowelsPercentage = word.length() / vowelCount;
            }
        }
        return vowelsPercentage;
    }

    //Надрукувати слова тексту в алфавітному порядку по першій букві.
    public static List<String> sortAlphabetical(List<String> text) {
        List<String> result = new ArrayList<>();
        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            Collections.sort(words);
            result.addAll(words);
        }
        Collections.sort(result);
        return result;
    }

    //Слова тексту, що починаються з голосних букв, відсортувати в алфавітному порядку по першій приголосній букві слова.
    public static List<String> sortByFirstConsonantLetter(List<String> text) {
        List<String> wordsStartsWithVowel = new ArrayList<>();
        Pattern pattern = Pattern.compile("^[аеоуиі|АЕОИУІ]");
        Matcher matcher;
        for (String sentence : text) {
            String[] words = sentence.split(" ");
            for (String w : words) {
                matcher = pattern.matcher(w);
                if (matcher.lookingAt()) {
                    wordsStartsWithVowel.add(w);
                }
            }
        }
        wordsStartsWithVowel.sort(new FirstConsonantLetterComparator());
        return wordsStartsWithVowel;
    }

    static class FirstConsonantLetterComparator implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            o1 = o1.replaceAll("[аеоуиіАЕОИУІ]", "");
            o2 = o2.replaceAll("[аеоуиіАЕОИУІ]", "");
            return o1.compareTo(o2);
        }
    }

    //Всі слова тексту відсортувати за зростанням кількості заданої букви у слові.
    // Слова з однаковою кількістю розмістити у алфавітному порядку.
    public static List<String> sortByLetterGrowth(List<String> text, char letter) {
        List<String> sorted = new ArrayList<>();
        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            words.sort(new LetterOccurrenceComparator(letter));
            sorted.addAll(words);
        }
        sorted.sort(new LetterOccurrenceComparator(letter));
        return sorted;
    }

    static class LetterOccurrenceComparator implements Comparator<String> {
        char letter;

        public LetterOccurrenceComparator(char letter) {
            this.letter = letter;
        }

        @Override
        public int compare(String o1, String o2) {
            long o1LetterCount = o1
                    .chars()
                    .filter(ch -> ch == letter)
                    .count();

            long o2LetterCount = o2
                    .chars()
                    .filter(ch -> ch == letter)
                    .count();
            if (o1LetterCount > o2LetterCount) {
                return -1;
            } else if (o1LetterCount == o2LetterCount) {
                return 0;
            }
            return 1;
        }
    }

    //Є текст і список слів. Для кожного слова з заданого списку знайти, скільки
//разів воно зустрічається у кожному реченні, і відсортувати слова за
//спаданням загальної кількості входжень.
    public static Set<String> sortByWordOccurrence(List<String> text, List<String> wordsToCount) {
        Map<String, Long> wordCount = new HashMap<>();
        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            for (String wordToCount : wordsToCount) {
                long count = words.stream().filter(w -> w.equalsIgnoreCase(wordToCount)).count();
                wordCount.put(wordToCount, count);
            }
        }
        wordCount = sortMapByValue(wordCount);
        return wordCount.keySet();
    }

    private static <K, V extends Comparable<? super V>> Map<K, V> sortMapByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());
        Collections.reverse(list);
        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    //З тексту видалити всі слова заданої довжини, що починаються на приголосну букву.
    public static List<String> removeWordWithSpecificLength(List<String> text, int length) {
        List<String> result = new ArrayList<>();
        Predicate<String> predicate = Pattern.compile("^[^аеоуиіАУОЕИІ]").asPredicate();

        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            List<String> filtered = words
                    .stream()
                    .filter(predicate)
                    .filter(w -> w.length() != length)
                    .collect(Collectors.toList());
            result.addAll(filtered);
        }
        return result;
    }

//Відсортувати слова у тексті за спаданням кількості входжень заданого символу,
// а у випадку рівності – за алфавітом.

    public static List<String> sortWordsByLetterOccurrence(List<String> text, char letter) {
        List<String> result = sortByLetterGrowth(text, letter);
        result.sort(new LetterOccurrenceAlphabeticComparator(letter));
        return result;
    }

    static class LetterOccurrenceAlphabeticComparator implements Comparator<String> {
        char letter;

        public LetterOccurrenceAlphabeticComparator(char letter) {
            this.letter = letter;
        }

        @Override
        public int compare(String o1, String o2) {
            long o1LetterCount = o1
                    .chars()
                    .filter(ch -> ch == letter)
                    .count();

            long o2LetterCount = o2
                    .chars()
                    .filter(ch -> ch == letter)
                    .count();
            if (o1LetterCount > o2LetterCount) {
                return -1;
            } else if (o1LetterCount == o2LetterCount) {
                return 0;
            }
            return 1;
        }
    }

    //Видалити з слова всі наступні входження першої букви цього слова
      public static List<String> removeLetters(List<String> text) {
        List<String> result = new ArrayList<>();
        for (String sentence : text) {
            String[] words = sentence.split(" ");
            for (String word : words) {
                char letter = 'a';
                if (word.length() > 0) {
                    letter = word.charAt(0);
                }
                word = word.replaceAll(String.valueOf(letter), "");
                result.add(word);
            }
        }
        return result;
    }


    //У певному реченні тексту слова заданої довжини замінити вказаним
    //підрядком, довжина якого може не співпадати з довжиною слова.
    public static List<String> replaceWithSubstring(List<String> text, int length, String substring) {
        List<String> result = new ArrayList<>();
        for (String sentence : text) {
            String[] words = sentence.split(" ");
            for (String word : words) {
                if (word.length() == length && word.length() != substring.length()) {
                    word = word.replace(substring, "");
                    result.add(word);
                }
            }
        }
        return result;
    }
//Перевірити чи слово є паліндром

    public static void isPalindrome(String text)  {
        text = text.replaceAll("\\W", "");
        StringBuilder strBuilder = new StringBuilder(text);
        strBuilder.reverse();
        String invertedText = strBuilder.toString();

        if (text.equalsIgnoreCase(invertedText)) {
            System.out.println("Yes, it is palindrome!");
        } else {
            System.out.println("Exception: your text is not palindrome :(");
        }
    }
}

